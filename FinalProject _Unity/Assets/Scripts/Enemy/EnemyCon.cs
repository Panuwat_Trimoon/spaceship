﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCon : MonoBehaviour
{
    GameObject scoreUIText;

    public GameObject Explosion;



    float speed = 2f;
    // Start is called before the first frame update
    void Start()
    {
        scoreUIText = GameObject.FindGameObjectWithTag("ScoreText");
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 position = transform.position;

        position = new Vector2(position.x, position.y - speed * Time.deltaTime);

        transform.position = position;

        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        if (transform.position.y < min.y)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if ((col.tag == "Player") || (col.tag == "PlayerBullet"))
        {
            EnemyExplosion();

            scoreUIText.GetComponent<GameScore>().Score += 10;

            Destroy(gameObject);
        }
    }

    private void EnemyExplosion()
    {
        GameObject explosion = (GameObject)Instantiate(Explosion);

        explosion.transform.position = transform.position;
    }
}
