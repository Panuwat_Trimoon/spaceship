﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject playButton;
    public GameObject player;
    public GameObject enemySpawner;
    public GameObject GameOver;
    public GameObject scoreUIText;
    public GameObject logo;
    public GameObject finalScoreText;

    
    public enum GameManagerState
    { 

         Opening,
         Gameplay,
         GameOver,
    }
    GameManagerState GMState;

    // Start is called before the first frame update
    void Start()
    {
        
        GMState = GameManagerState.Opening;
    }

    // Update is called once per frame
    void UpdateGameManagerState()
    {
        switch(GMState)
        {
            case GameManagerState.Opening:

                logo.SetActive(true);

                GameOver.SetActive(false);

                playButton.SetActive(true);
                break;
            case GameManagerState.Gameplay:

                logo.SetActive(false);

                scoreUIText.GetComponent<GameScore>().Score = 0;

                playButton.SetActive(false);

                player.GetComponent<PlayerCon>().Init();

                enemySpawner.GetComponent<EnemySpawner>().ScheduleEnemySpawner();
                break;

            case GameManagerState.GameOver:

                

                enemySpawner.GetComponent<EnemySpawner>().UnscheduleEnemySpawner();

                GameOver.SetActive(true);

                
               
                break;
                
        }
    }

    public void SetGameManagerState(GameManagerState state)
    {
        GMState = state;
        UpdateGameManagerState();
    }

    public void StartGamePlay()
    {
        GMState = GameManagerState.Gameplay;
        UpdateGameManagerState();
    }

    public void ChangeToOpeningState()
    {
        SetGameManagerState(GameManagerState.Opening);
    }
}
